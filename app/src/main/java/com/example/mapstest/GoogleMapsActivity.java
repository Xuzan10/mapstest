package com.example.mapstest;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.example.mapstest.api.ApiClient;
import com.example.mapstest.api.ApiInterface;
import com.example.mapstest.utils.PermissionHelper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class GoogleMapsActivity extends FragmentActivity implements OnMapReadyCallback {

    PermissionHelper permissionHelper;
    ApiInterface apiInterface;
    private GoogleMap mMap;
    private MarkerOptions options = new MarkerOptions();
    private ArrayList<LatLng> latlngs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        String[] permissionList = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        permissionHelper = new PermissionHelper(
                this,
                null,
                permissionList
        );
        permissionHelper.checkAndRequestPermissions();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

       /* Call<List<LatLngModel>> call = apiInterface.getListOfLatLng();
        call.enqueue(new Callback<List<LatLngModel>>() {
            @Override
            public void onResponse(Call<List<LatLngModel>> call, Response<List<LatLngModel>> response) {
                Log.d("print", "onResponse: " + response.body());
            }

            @Override
            public void onFailure(Call<List<LatLngModel>> call, Throwable t) {

            }
        });*/

        apiInterface.getListOfLatLng()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<LatLngModel>>() {
                    @Override
                    public void onSubscribe(Subscription s) {
                        s.request(Long.MAX_VALUE);
                    }

                    @Override
                    public void onNext(List<LatLngModel> latLngModelList) {
                        Toast.makeText(GoogleMapsActivity.this, latLngModelList.toString(), Toast.LENGTH_LONG).show();
                        for (LatLngModel latLngModel : latLngModelList) {
                            latlngs.add(new LatLng(latLngModel.getLatitude(), latLngModel.getLongitude()));
                        }
                        addMarker();
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    private void addMarker() {
        for (LatLng point : latlngs) {
            options.position(point);
            options.title("someTitle");
            options.snippet("someDesc");
            mMap.addMarker(options);
        }
        if (!latlngs.isEmpty()) {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latlngs.get(0).latitude, latlngs.get(0).longitude)));
            //    mMap.animateCamera(CameraUpdateFactory.zoomIn());
            // Zoom out to zoom level 10, animating with a duration of 2 seconds.
            mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        permissionHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera

    }
}
