package com.example.mapstest.api;

import com.example.mapstest.LatLngModel;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("bins/iv54y")
    Flowable<List<LatLngModel>> getListOfLatLng();

}
