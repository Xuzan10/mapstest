package com.example.mapstest.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PermissionHelper {
    public static final String TAG = "PermissionsHelper";
    public static final int RESULT_ACTIVITY_CODE = 123;
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 100; // any code you want.
    private Activity activity;
    private Fragment fragment;
    private String[] permissions;
    private PermissionGrantedCallBack permissionGrantedCallBackListener;

    // Called if all permissions are already present or user grants all permissions.
    public interface PermissionGrantedCallBack {
        void onAllPermissionGranted();
    }

    /*
     * If PermissionHelper is called from an Activity, fragment is set to null.
     * If PermissionHelper is called from Fragment, both Activity and Fragment is set.
     */
    public PermissionHelper(Activity activity, Fragment fragment, String[] permissions) {
        this.activity = activity;
        this.permissions = permissions;
        this.fragment = fragment;
    }

    public void checkAndRequestPermissions() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        Log.d(TAG, Arrays.toString(permissions));
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(permission);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            if (fragment == null) {
                ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            } else {
                fragment.requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        } else {
            if (permissionGrantedCallBackListener != null) {
                permissionGrantedCallBackListener.onAllPermissionGranted();
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();

                for (String permission : permissions) {
                    perms.put(permission, PackageManager.PERMISSION_GRANTED);
                }

                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);

                    boolean allPermissionsGranted = true;
                    for (String permission1 : permissions) {
                        allPermissionsGranted = allPermissionsGranted && (perms.get(permission1) == PackageManager.PERMISSION_GRANTED);
                    }

                    if (allPermissionsGranted) {
                        Log.d(PermissionHelper.class.getSimpleName(), "onRequestPermissionsResult: all permissions granted");
                        if (permissionGrantedCallBackListener != null) {
                            permissionGrantedCallBackListener.onAllPermissionGranted();
                        }
                    } else {
                        StringBuilder message = new StringBuilder("Permission not granted");

                        message.append("\n\n" + "Please provide permission");

                        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                        builder.setTitle("Permission Required")
                                .setMessage(message)
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        checkRationale();
                                        dialog.dismiss();
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                }
            }
        }
    }

    private void checkRationale() {
        boolean showRational = false;

        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                showRational = true;
            }
        }

        if (showRational) {
            checkAndRequestPermissions();
        } else {
            Toast.makeText(activity, "Please enable permissions from application settings", Toast.LENGTH_LONG)
                    .show();
            Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData(Uri.parse("package:" + activity.getPackageName()));
            activity.startActivityForResult(intent, RESULT_ACTIVITY_CODE);
        }
    }

    public boolean isAllPermissionAvailable() {
        for (int i = 0, permissionsLength = permissions.length; i < permissionsLength; i++) {
            String permission = permissions[i];
            int result = ContextCompat.checkSelfPermission(activity, permission);
            if (result != PackageManager.PERMISSION_GRANTED) return false;
        }
        return true;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_ACTIVITY_CODE) {
            if (!isAllPermissionAvailable()) {
                checkAndRequestPermissions();
            }
        }
    }

    public void setPermissionGrantedCallBackListener(PermissionGrantedCallBack permissionGrantedCallBackListener) {
        this.permissionGrantedCallBackListener = permissionGrantedCallBackListener;
    }
}
